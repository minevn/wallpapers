package net.minevn.wallpapers

import org.bukkit.entity.Player
import org.bukkit.map.MapCanvas
import org.bukkit.map.MapRenderer
import org.bukkit.map.MapView

class ImageRenderer(private val image: WallImage) : MapRenderer() {
	override fun render(view: MapView, canvas: MapCanvas, player: Player) {
		image.render(view.id.toInt(), canvas)
	}
}
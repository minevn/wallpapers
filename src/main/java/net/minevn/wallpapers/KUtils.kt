package net.minevn.wallpapers

import de.tr7zw.nbtapi.NBTItem
import org.bukkit.Material
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack

fun ItemStack.setName(name: String) = this.apply {
	val im = itemMeta ?: return@apply
	im.setDisplayName(name)
	itemMeta = im
}

fun ItemStack.setLores(lores: List<String>) = this.apply {
	val im = itemMeta ?: return@apply
	im.lore = lores
	itemMeta = im
}

fun ItemStack.hideThings() = this.apply {
	val im = itemMeta ?: return@apply
	im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES)
	itemMeta = im
}

fun ItemStack.getWallpaper(): WallImage? = NBTItem(this)
	.getString("wallpapers_id")
	.let { WallImage[it] }

fun ItemStack.getWallpaperCount() = NBTItem(this).getInteger("wallpapers_count") ?: 0

fun ItemStack?.isNullOrAir() = this == null || this.type == Material.AIR

fun ItemStack.increaseWallpaperCount() = this.run {
	val wp = this.getWallpaper() ?: throw IllegalStateException("image not found")
	val count = (getWallpaperCount() + 1).takeIf { it < wp.getSize() } ?: return@run true
	NBTItem(this, true).setInteger("wallpapers_count", count)
	setName("§6wallpaper ${wp.id} §a${count + 1}/${wp.getSize()} (${wp.width} x ${wp.height})")
	false
}

fun List<String>.tabComplete(prefix: String) = this
	.filter { it.startsWith(prefix) }
	.sorted()
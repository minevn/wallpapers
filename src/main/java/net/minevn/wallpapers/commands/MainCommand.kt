package net.minevn.wallpapers.commands

import net.minevn.wallpapers.Wallpapers
import net.minevn.wallpapers.commands.subcommands.CreateCommand
import net.minevn.wallpapers.commands.subcommands.GetPlacerCommand
import net.minevn.wallpapers.commands.subcommands.ReplaceCommand
import net.minevn.wallpapers.tabComplete
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor

class MainCommand : TabExecutor {
	private val main = Wallpapers.instance!!
	private val subCommands = mutableMapOf<String, SubCommand>()

	init {
		CreateCommand(this)
		GetPlacerCommand(this)
		ReplaceCommand(this)
	}

	fun registerSubCommand(label: String, command: SubCommand) {
		subCommands[label] = command
	}

	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
		if (args.isNotEmpty()) {
			subCommands[args[0]]?.let{
				it.onCommand(sender, command, label, args)
				return@onCommand true
			}
		}

		subCommands.entries
			.distinctBy { it.value }
			.forEach {
				sender.sendMessage("§e/$label ${it.key}${it.value.usage()}§f: ${it.value.description}")
			}
		return true
	}

	override fun onTabComplete(
		sender: CommandSender,
		command: Command,
		alias: String,
		args: Array<out String>
	): List<String> {
		if (args.isNotEmpty()) {
			if (args.size == 1) return subCommands.keys.toList().tabComplete(args[0])
			return subCommands[args[0]]?.onTabComplete(sender, command, alias, args)!!
		}
		return listOf()
	}
}
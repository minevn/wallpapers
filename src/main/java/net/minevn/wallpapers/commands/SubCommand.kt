package net.minevn.wallpapers.commands

import org.bukkit.command.Command
import org.bukkit.command.CommandSender

abstract class SubCommand(parent: MainCommand, val description: String, vararg alias: String) {
	init {
		alias.forEach { parent.registerSubCommand(it, this) }
	}

	abstract fun onCommand(
		sender: CommandSender,
		command: Command,
		label: String,
		args: Array<out String>?
	)

	open fun onTabComplete(
		sender: CommandSender,
		command: Command,
		alias: String,
		args: Array<out String>?
	) = listOf<String>()

	open fun usage() = ""
}
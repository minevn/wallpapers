package net.minevn.wallpapers.commands.subcommands

import net.minevn.wallpapers.WallImage
import net.minevn.wallpapers.WallImage.Companion.generateBytes
import net.minevn.wallpapers.WallImage.Companion.saveImage
import net.minevn.wallpapers.Wallpapers.Companion.runNotSync
import net.minevn.wallpapers.commands.MainCommand
import net.minevn.wallpapers.commands.SubCommand
import net.minevn.wallpapers.tabComplete
import org.bukkit.command.Command
import org.bukkit.command.CommandSender

class ReplaceCommand(parent: MainCommand) : SubCommand(
	parent, "Thay thế ảnh từ ID có sẵn", "replace"
) {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>?) {
		if (args == null) return
		if (args.size < 3) {
			sender.sendMessage("/$label replace${usage()}")
			return
		}
		val id = args[1]
		val url = args[2]
		val image = WallImage[id] ?: run {
			sender.sendMessage("ID hinh $id khong ton tai")
			return@onCommand
		}
		sender.sendMessage("Dang cap nhat hinh...")

		val w = image.width * 128
		val h = image.height * 128
		val oneD = ByteArray(w * h)
		val twoD = Array(h) { ByteArray(w) }

		runNotSync {
			generateBytes(url, image.width, image.height, twoD, oneD)
			saveImage(id, oneD)
			image.image = twoD
			image.rendered.clear()
			sender.sendMessage("Da cap nhat hinh cho id $id")
		}
	}

	override fun onTabComplete(
		sender: CommandSender,
		command: Command,
		alias: String,
		args: Array<out String>?
	): List<String> {
		if (args?.size == 2) {
			return WallImage.getSuggestions().tabComplete(args[1])
		}
		return mutableListOf()
	}

	override fun usage(): String = " <ID ảnh> <URL ảnh>"
}
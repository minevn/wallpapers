package net.minevn.wallpapers.commands.subcommands

import net.minevn.wallpapers.WallImage
import net.minevn.wallpapers.commands.MainCommand
import net.minevn.wallpapers.commands.SubCommand
import org.bukkit.command.Command
import org.bukkit.command.CommandSender

class CreateCommand(parent: MainCommand) : SubCommand(parent, "Tạo ảnh mới", "create", "taoanh") {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>?) {
		if (args == null) return
		if (args.size < 4) {
			sender.sendMessage("/$label create${usage()}")
			return
		}
		val id = args[1]
		if (WallImage.isExists(id)) {
			sender.sendMessage("ID hinh $id da ton tai")
			return
		}
		val url = args[2]
		val size = args[3].split(":")
		val width = size[0].toInt()
		val height = size[1].toInt()
		WallImage.createImage(id, url, width, height)
		sender.sendMessage("Image created successfully")
	}

	override fun usage(): String = " <id> <url> <blockWidth>:<blockHeight>"
}
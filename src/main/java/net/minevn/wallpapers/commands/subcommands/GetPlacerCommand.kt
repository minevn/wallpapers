package net.minevn.wallpapers.commands.subcommands

import net.minevn.wallpapers.WallImage
import net.minevn.wallpapers.commands.MainCommand
import net.minevn.wallpapers.commands.SubCommand
import net.minevn.wallpapers.tabComplete
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class GetPlacerCommand(parent: MainCommand) : SubCommand(
	parent, "Lấy tool treo hình lên tường", "getplacer", "place"
) {
	override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>?) {
		val player = sender as? Player ?: run {
			sender.sendMessage("must be player")
			return@onCommand
		}
		val id = args?.getOrNull(1) ?: run {
			sender.sendMessage("/$label place${usage()}")
			return@onCommand
		}
		val image = WallImage[id] ?: run {
			sender.sendMessage("image not found")
			return@onCommand
		}
		player.inventory.addItem(image.getItem())
		player.sendMessage("Item given")
	}

	override fun onTabComplete(
		sender: CommandSender,
		command: Command,
		alias: String,
		args: Array<out String>?
	): List<String> {
		if (args?.size == 2) {
			return WallImage.getSuggestions().tabComplete(args[1])
		}
		return mutableListOf()
	}

	override fun usage() = " <map id>"
}
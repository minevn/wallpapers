package net.minevn.wallpapers

import de.tr7zw.nbtapi.NBTItem
import net.minevn.wallpapers.Wallpapers.Companion.runSync
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.inventory.ItemStack
import org.bukkit.map.MapCanvas
import org.bukkit.map.MapPalette
import java.awt.Color
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.URL
import javax.imageio.ImageIO

class WallImage(
	val id: String,
	var image: Array<ByteArray>,
	private val listMapID: List<Int>
) {
	var rendered = mutableListOf<Int>()
	private val positionByID = listMapID.withIndex().associate { it.value to it.index  }
	val width = image[0].size / 128
	val height = image.size / 128

	init {
		runSync {
			positionByID.keys.forEach { id ->
				val map = Bukkit.getMap(id) ?: return@forEach
				map.renderers.forEach {
					map.removeRenderer(it)
				}
				map.addRenderer(ImageRenderer(this))
			}
		}

		list[id] = this
	}

	fun render(id: Int, canvas: MapCanvas) {
		if (rendered.contains(id)) return
		val pos = positionByID[id]!!
		if (pos < 0) return
		val x = pos % width
		val y = pos / width
		for (a in 0 .. 127) {
			for (b in 0 .. 127) {
				canvas.setPixel(a, b, image[b + 128 * y][a + 128 * x])
			}
		}
		rendered.add(id)
	}

	fun getSize() = positionByID.size

	fun getMapId(pos: Int) = listMapID[pos]

	fun getItem() = ItemStack(Material.PAPER)
		.setName("§6wallpaper $id §a1/${getSize()} ($width x $height)")
		.setLores(listOf(
			"§bLần luợt click phải vào",
			"§bItem frame để đặt"
		))
		.also {
			NBTItem(it, true).setString("wallpapers_id", id)
		}

	companion object {
		private val list = mutableMapOf<String, WallImage>()

		fun load() {
			list.clear()
			val main = Wallpapers.instance!!
			main.log("Loading images...")
			val folder = File(main.dataFolder, "images")
			if (!folder.exists() || !folder.isDirectory) {
				main.warning("images folder is not valid")
				return
			}
			folder.listFiles()?.forEach { current ->
				try {
					val id = current.name
					val config = YamlConfiguration.loadConfiguration(File(current, "info.yml"))
					val dataFile = File(current, "data.bin")
					val width = config.getInt("width")
					val height = config.getInt("height")
					val ids = config.getIntegerList("ids")
					val bytes = loadImage(dataFile, width, height)
					WallImage(id, bytes, ids)
				} catch (e: Exception) {
					main.error(e, "Can not load file ${current.name}")
				}
			}
			main.log("Loaded ${list.size} images")
		}

		fun generateBytes(
			imageUrl: String, blockWidth: Int, blockHeight: Int,
			twoD: Array<ByteArray>?, oneD: ByteArray?
		) {
			val w = blockWidth * 128
			val h = blockHeight * 128
			val img = ImageIO.read(URL(imageUrl))!!
			val tmp = img.getScaledInstance(w, h, Image.SCALE_SMOOTH)
			val dimg = BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
			val g2d = dimg.createGraphics()
			g2d.drawImage(tmp, 0, 0, null)
			g2d.dispose()
			for (x in 0 until h) {
				for (y in 0 until w) {
					val value = MapPalette.matchColor(Color(dimg.getRGB(y, x), true))
					if (twoD != null) twoD[x][y] = value
					if (oneD != null) oneD[x + y * h] = value
				}
			}
		}

		fun saveImage(id: String, oneD: ByteArray) {
			val main = Wallpapers.instance!!
			val folder = File(main.dataFolder, "images")
			if (!folder.exists()) folder.mkdir()
			if (!folder.isDirectory) return
			val current = File(folder, id)
			if (current.exists()) current.delete()
			current.mkdir()
			val file = File(current, "data.bin")
			if (file.exists()) {
				file.delete()
				file.createNewFile()
			}
			FileOutputStream(file).use {
				it.write(oneD)
			}
		}

		fun createImage(id: String, imageUrl: String, blockWidth: Int, blockHeight: Int) {
			val main = Wallpapers.instance!!
			Wallpapers.runNotSync {
				main.log("Loading $imageUrl ${blockWidth}x$blockHeight blocks")
				val w = blockWidth * 128
				val h = blockHeight * 128
				val twoD = Array(h) { ByteArray(w) }
				val oneD = ByteArray(w * h)
				generateBytes(imageUrl, blockWidth, blockHeight, twoD, oneD)
				saveImage(id, oneD)
				val folder = File(main.dataFolder, "images")
				if (!folder.exists()) folder.mkdir()
				if (!folder.isDirectory) return@runNotSync
				val current = File(folder, id)
				val configFile = File(current, "info.yml")
				val mapIds = mutableListOf<Int>()
				for (x in 0 until blockWidth * blockHeight) {
					val map = Bukkit.createMap(Bukkit.getWorlds()[0])
					mapIds.add(map.id)
				}
				val config = YamlConfiguration()
				config.set("width", blockWidth)
				config.set("height", blockHeight)
				config.set("ids", mapIds)
				config.save(configFile)
				runSync {
					WallImage(id, twoD, mapIds)
					main.log("Saved image $id")
				}
			}
		}

		private fun loadImage(file: File, blockWidth: Int, blockHeight: Int): Array<ByteArray> {
			if (!file.exists()) throw IllegalStateException("file not exists")
			val w = blockWidth * 128
			val h = blockHeight * 128
			val image = Array(h) { ByteArray(w) }
			FileInputStream(file).use {
				val oneD = it.readBytes()
				for (x in 0 until h) {
					for (y in 0 until w) {
						image[x][y] = oneD[x + y * h]
					}
				}
			}
			return image
		}

		fun isExists(id: String) = list.containsKey(id)

		operator fun get(id: String) = list[id]

		fun getSuggestions() = list.keys.toMutableList()
	}
}
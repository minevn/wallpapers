package net.minevn.wallpapers

import net.minevn.wallpapers.commands.MainCommand
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.ItemFrame
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.MapMeta
import org.bukkit.plugin.java.JavaPlugin
import java.util.logging.Level

class Wallpapers : JavaPlugin(), Listener {
	override fun onEnable() {
		instance = this
		getCommand("wallpapers")!!.run {
			val mainCmd = MainCommand()
			setExecutor(mainCmd)
			tabCompleter = mainCmd
		}
		runNotSync { WallImage.load() }
		server.pluginManager.registerEvents(this, this)
	}

	fun log(message: String) = logger.info(message)

	fun warning(message: String) = logger.warning(message)

	fun error(e: Exception, message: String) = logger.log(Level.SEVERE, message, e)

	@EventHandler(ignoreCancelled = true)
	fun onPlace(e: PlayerInteractEntityEvent) {
		val frame = (e.rightClicked as? ItemFrame)?.takeIf { it.isEmpty } ?: return
		val item = e.player.inventory.itemInMainHand?.takeIf { !it.isNullOrAir() }
		val image = item?.getWallpaper() ?: return
		val count = item.getWallpaperCount()
		val id = image.getMapId(count)
		val map = ItemStack(Material.FILLED_MAP, 1)
		val im = map.itemMeta as? MapMeta ?: return
		im.mapId = id
		map.itemMeta = im
		e.isCancelled = true
		frame.setItem(map)
		if (item.increaseWallpaperCount()) {
			e.player.inventory.remove(item)
		}
		e.player.sendMessage("Placed image #${count + 1} with ID $id")
	}

	companion object {
		var instance: Wallpapers? = null
			private set

		fun runAsync(r: Runnable) = Bukkit.getScheduler().runTaskAsynchronously(instance!!, r)

		fun runForceSync(r: Runnable) = Bukkit.getScheduler().runTask(instance!!, r)

		fun runSync(r: Runnable) {
			if (Bukkit.isPrimaryThread()) r.run()
			else runForceSync(r)
		}

		fun runNotSync(r: Runnable) {
			if (!Bukkit.isPrimaryThread()) r.run()
			else runAsync(r)
		}
	}
}